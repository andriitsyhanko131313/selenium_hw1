import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SimpleTest extends BaseTest {
    @Test(description = "verifies opening 'Images' tab")
    public void simpleTest() {
        driver.get(BASE_URL);
        WebElement inputField = driver.findElement(By.name("q"));
        inputField.sendKeys(SEARCH_KEYWORD);
        inputField.submit();
        WebDriverWait wait = new WebDriverWait(driver,EXPLICIT_TIME);
        Assert.assertTrue(wait.until(dr -> dr.getTitle().startsWith(SEARCH_KEYWORD)));

        WebElement imaginesTab = wait
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@data-hveid='CAIQAw' or @data-hveid='CAEQAw']")));
        imaginesTab.click();

        Assert.assertTrue(wait
                .until(dr -> dr.findElement(By.xpath("//span[@class='rQEFy NZmxZe']")).isDisplayed()));
    }
}
